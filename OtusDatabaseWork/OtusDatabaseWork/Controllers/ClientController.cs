﻿namespace OtusDatabaseWork
{
    using System;
    using System.Collections.Generic;
    using OtusDatabaseWork.Entities;
    using OtusDatabaseWork.Interface;
    using OtusDatabaseWork.Repositories;

    public class ClientController : IController
    {
        private readonly ClientRepository ClientRepository;

        public ClientController(ClientRepository client)
        {
            ClientRepository = client;
        }

        public void Show()
        {
            var allClients = ClientRepository.GetAll();
            Print(allClients, "Client table");
        }

        private void Print(IList<Client> collections, string title = null)
        {
            if (!string.IsNullOrEmpty(title))
                Console.WriteLine($"{title}");

            Console.WriteLine(string.Join(Environment.NewLine, collections));
            Console.WriteLine();
        }
    }
}
