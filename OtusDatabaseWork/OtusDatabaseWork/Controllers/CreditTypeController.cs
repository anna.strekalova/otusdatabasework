﻿namespace OtusDatabaseWork
{
    using System;
    using System.Collections.Generic;
    using OtusDatabaseWork.Entities;
    using OtusDatabaseWork.Interface;
    using OtusDatabaseWork.Repositories;

    public class CreditTypeController : IController
    {
        private readonly CreditTypeRepository CreditTypeRepository;

        public CreditTypeController(CreditTypeRepository creditType)
        {
            CreditTypeRepository = creditType;
        }

        public void Show()
        {
            var allCreditTypes = CreditTypeRepository.GetAll();
            Print(allCreditTypes, "Credit type table");
        }

        private void Print(IList<CreditType> collections, string title = null)
        {
            if (!string.IsNullOrEmpty(title))
                Console.WriteLine($"{title}");

            Console.WriteLine(string.Join(Environment.NewLine, collections));
            Console.WriteLine();
        }
    }
}
