﻿namespace OtusDatabaseWork
{
    using System;
    using System.Collections.Generic;
    using OtusDatabaseWork.Entities;
    using OtusDatabaseWork.Interface;
    using OtusDatabaseWork.Repositories;

    public class CreditController : IController
    {
        private readonly CreditRepository CreditRepository;

        public CreditController(CreditRepository credit)
        {
            CreditRepository = credit;
        }
        public void Show()
        {
            var allCredits = CreditRepository.GetAll();
            Print(allCredits, "Credit table");
        }

        private void Print(IList<Credit> collections, string title = null)
        {
            if (!string.IsNullOrEmpty(title))
                Console.WriteLine($"{title}");

            Console.WriteLine(string.Join(Environment.NewLine, collections));
            Console.WriteLine();
        }
    }
}
