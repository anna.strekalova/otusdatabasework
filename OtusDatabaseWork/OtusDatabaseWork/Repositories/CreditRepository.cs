﻿namespace OtusDatabaseWork.Repositories
{
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Data;
    using Dapper;
    using Npgsql;
    using OtusDatabaseWork.Entities;
    using OtusDatabaseWork.Interface;

    public class CreditRepository : ICreditRepository
    {
        private string _connectionString { get; }

        public CreditRepository(string connect)
        {
            _connectionString = connect;
        }

        public int Create(Credit entity)
        {
            if (entity == null)
                return 0; ;

            using (IDbConnection db = new NpgsqlConnection(_connectionString))
            {
                var query = @"INSERT INTO credits (ClientId, CreditTypeId, Amount, DateOfIssue, IsRepaid) 
                            VALUES (@ClientId, @CreditTypeId, @Amount, @DateOfIssue, @IsRepaid)";
                return db.Execute(query, entity);
            }
        }

        public IList<Credit> GetAll()
        {
            using (IDbConnection db = new NpgsqlConnection(_connectionString))
            {
                var query = @"SELECT * FROM credits 
                            JOIN clients ON credits.ClientId = clients.Id
                            JOIN credit_types ON credits.CreditTypeId = credit_types.Id";
                var result = db.Query<Credit, Client, CreditType, Credit>(query, (credit, client, creditType) =>
                {
                    credit.Client = client;
                    credit.CreditType = creditType;
                    return credit;
                });
                return result.ToImmutableList();
            }
        }
    }
}
