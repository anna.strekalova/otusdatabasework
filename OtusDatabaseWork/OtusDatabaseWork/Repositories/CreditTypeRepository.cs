﻿namespace OtusDatabaseWork.Repositories
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Dapper;
    using Npgsql;
    using OtusDatabaseWork.Entities;
    using OtusDatabaseWork.Interface;

    public class CreditTypeRepository : ICreditTypeRepository
    {
        private string _connectionString { get; }

        public CreditTypeRepository(string connect)
        {
            _connectionString = connect;
        }

        public int Create(CreditType entity)
        {
            if (entity == null)
                return 0; ;

            using (IDbConnection db = new NpgsqlConnection(_connectionString))
            {
                var query = @"INSERT INTO credit_types (Name, Description, Rate, TermDays) 
                            VALUES (@Name, @Description, @Rate, @TermDays)";
                return db.Execute(query, entity);
            }
        }

        public IList<CreditType> GetAll()
        {
            using (IDbConnection db = new NpgsqlConnection(_connectionString))
            {
                var query = @"SELECT * FROM credit_types";
                return db.Query<CreditType>(query).ToList();
            }
        }

        public CreditType GetByName(string name)
        {
            using (IDbConnection db = new NpgsqlConnection(_connectionString))
            {
                var query = $"SELECT * FROM credit_types WHERE credit_types.Name = @name";
                return db.Query<CreditType>(query, new { name}).FirstOrDefault();
            }
        }
    }
}
