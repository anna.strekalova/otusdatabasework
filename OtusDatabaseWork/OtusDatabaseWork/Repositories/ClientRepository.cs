﻿namespace OtusDatabaseWork.Repositories
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Dapper;
    using Npgsql;
    using OtusDatabaseWork.Entities;
    using OtusDatabaseWork.Interface;

    public class ClientRepository : IClientRepository
    {
        private string _connectionString { get; }

        public ClientRepository(string connect)
        {
            _connectionString = connect;
        }

        public int Create(Client entity)
        {
            if (entity == null)
                return 0; ;

            using (IDbConnection db = new NpgsqlConnection(_connectionString))
            {
                var query = @"INSERT INTO clients (FirstName, MiddleName, LastName, Address) 
                            VALUES (@FirstName, @MiddleName, @LastName, @Address)";
                return db.Execute(query, entity);
            }
        }

        public IList<Client> GetAll()
        {
            using (IDbConnection db = new NpgsqlConnection(_connectionString))
            {
                var query = @"SELECT * FROM clients";
                return db.Query<Client>(query).ToList();
            }
        }
        public Client GetByName(string firstName, string middleName, string lastName)
        {
            using (IDbConnection db = new NpgsqlConnection(_connectionString))
            {
                var query = @"SELECT * FROM clients 
                            WHERE clients.FirstName = @firstName 
                            AND clients.MiddleName = @middleName
                            AND clients.LastName = @lastName";
                return db.Query<Client>(query, new { firstName, middleName, lastName}).FirstOrDefault();
            }
        }
    }
}
