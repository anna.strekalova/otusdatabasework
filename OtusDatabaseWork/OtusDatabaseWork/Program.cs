﻿namespace OtusDatabaseWork
{
    using OtusDatabaseWork.Repositories;

    internal class Program
    {
        static void Main(string[] args)
        {
            string connnectionString = "Host=localhost;Database=bank;Username=otus;Password=otus";

            var clientRepository = new ClientRepository(connnectionString);
            var creditTypeRepository = new CreditTypeRepository(connnectionString);
            var creditRepository = new CreditRepository(connnectionString);

            var menu = new ConsoleMenu(creditRepository, creditTypeRepository, clientRepository);
            menu.ShowMenu();
        }
    }
}
