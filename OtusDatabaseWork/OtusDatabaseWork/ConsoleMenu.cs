﻿namespace OtusDatabaseWork
{
    using System;
    using OtusDatabaseWork.Entities;
    using OtusDatabaseWork.Repositories;

    public class ConsoleMenu
    {
        private readonly CreditRepository _creditRepository;
        private readonly CreditTypeRepository _creditTypeRepository;
        private readonly ClientRepository _clientRepository;

        public ConsoleMenu(CreditRepository creditRepository, CreditTypeRepository creditTypeRepository, ClientRepository clientRepository)
        {
            _creditRepository = creditRepository;
            _creditTypeRepository = creditTypeRepository;
            _clientRepository = clientRepository;
        }

        public void ShowMenu()
        {
            Console.WriteLine("Выберите действие:\n1 - вывести содержимое всех таблиц\n2 - добавить запись в таблицу");
            var option = Console.ReadLine();
            switch (option)
            {
                case "1":
                    ShowTables();
                    break;
                case "2":
                    Console.WriteLine("Выберите таблицу для добавления записи: 1 - Clients table, 2 - CreditTypes table, 3 - Credits table");
                    option = Console.ReadLine();
                    switch (option)
                    {
                        case "1":
                            {
                                ShowClientMenu();
                                break;
                            }
                        case "2":
                            {
                                ShowCreditTypeMenu();
                                break;
                            }
                        case "3":
                            ShowCreditMenu();
                            break;
                        default:
                            Console.WriteLine("Вы ввели неподдерживаемую команду.");
                            break;
                    }
                    break;
                default:
                    Console.WriteLine("Вы ввели неподдерживаемую команду.");
                    break;
            }
        }

        private void ShowCreditMenu()
        {
            Console.WriteLine("Название типа кредита:"); // внешний ключ
            var creditTypeStr = Console.ReadLine();
            var creditType = _creditTypeRepository.GetByName(creditTypeStr);

            Console.WriteLine("Фамилия клиента:"); // внешний ключ
            var firstName = Console.ReadLine();
            Console.WriteLine("Имя клиента:");
            var middleName = Console.ReadLine();
            Console.WriteLine("Отчество клиента:");
            var lastName = Console.ReadLine();
            var client = _clientRepository.GetByName(firstName, middleName, lastName);

            if (creditType != null && client != null)
            {
                Console.WriteLine("Сумма:");
                var amount = Console.ReadLine();
                Console.WriteLine("Дата выдачи:");
                var dateOfIssue = Console.ReadLine();
                try
                {
                    var result = _creditRepository.Create(new Credit(client.Id, creditType.Id, Decimal.Parse(amount), DateTime.Parse(dateOfIssue), false));
                    if (result > 0)
                        Console.WriteLine("Запись добавлена!");
                    else
                        Console.WriteLine("Запись не добавлена.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Ошибка. {ex.Message}");
                }
            }
            else
                Console.WriteLine("Невозможно создать кредит. Указанный вид кредита или пользователь не найден.");
        }

        private void ShowCreditTypeMenu()
        {
            Console.WriteLine("Название:");
            var name = Console.ReadLine();
            Console.WriteLine("Описание:");
            var description = Console.ReadLine();
            Console.WriteLine("Ставка:");
            var rate = Console.ReadLine();
            Console.WriteLine("Срок дней:");
            var termDays = Console.ReadLine();
            try
            {
                var result = _creditTypeRepository.Create(new CreditType(name, description, Decimal.Parse(rate), Convert.ToInt32(termDays)));
                if (result > 0)
                    Console.WriteLine("Запись добавлена!");
                else
                    Console.WriteLine("Запись не добавлена.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка. {ex.Message}");
            }
        }

        private void ShowClientMenu()
        {
            Console.WriteLine("Фамилия:");
            var firstName = Console.ReadLine();
            Console.WriteLine("Имя:");
            var middleName = Console.ReadLine();
            Console.WriteLine("Отчество:");
            var lastName = Console.ReadLine();
            Console.WriteLine("Адрес:");
            var address = Console.ReadLine();
            var result = _clientRepository.Create(new Client(firstName, middleName, lastName, address));
            if (result > 0)
                Console.WriteLine("Запись добавлена!");
            else
                Console.WriteLine("Запись не добавлена.");
        }

        private void ShowTables()
        {
            var clientController = new ClientController(_clientRepository);
            var creditTypeController = new CreditTypeController(_creditTypeRepository);
            var creditController = new CreditController(_creditRepository);

            clientController.Show();
            creditTypeController.Show();
            creditController.Show();
        }
    }
}
