﻿namespace OtusDatabaseWork.Interface
{
    using OtusDatabaseWork.Entities;

    public interface ICreditTypeRepository : IRepository<CreditType>
    {
    }
}
