﻿namespace OtusDatabaseWork.Interface
{
    using OtusDatabaseWork.Entities;

    public interface IClientRepository : IRepository<Client>
    {

    }
}
