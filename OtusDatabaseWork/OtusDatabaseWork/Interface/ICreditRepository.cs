﻿namespace OtusDatabaseWork.Interface
{
    using OtusDatabaseWork.Entities;

    public interface ICreditRepository : IRepository<Credit>
    {
    }
}
