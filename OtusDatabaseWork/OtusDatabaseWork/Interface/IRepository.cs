﻿namespace OtusDatabaseWork.Interface
{
    using System.Collections.Generic;

    public interface IRepository<T> where T : class
    {
        int Create(T obj);
        IList<T> GetAll();
    }
}
