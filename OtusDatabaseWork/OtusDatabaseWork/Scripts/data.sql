-------------------------------- Create tables ---------------------------------------------

CREATE TABLE clients(
	Id SERIAL UNIQUE PRIMARY KEY,
	FirstName VARCHAR(50) NOT NULL,
	MIddleName VARCHAR(50) NOT NULL,
	LastName VARCHAR(50) NOT NULL,
	Address VARCHAR(255) NOT NULL
);

CREATE TABLE credit_types(
	Id SERIAL UNIQUE PRIMARY KEY,
	Name VARCHAR(50) NOT NULL,
	Description TEXT,
	Rate DECIMAL NOT NULL,
	TermDays INT NOT NULL
);

CREATE TABLE credits(
	Id SERIAL UNIQUE PRIMARY KEY,
	ClientId INT NOT NULL,
	CreditTypeId INT NOT NULL,
	Amount DECIMAL NOT NULL,
	DateOfIssue TIMESTAMP NOT NULL,
	IsRepaid BOOLEAN NOT NULL,
	FOREIGN KEY (ClientId)
		REFERENCES clients (Id),
	FOREIGN KEY (CreditTypeId)
		REFERENCES credit_types (Id)	
);

-------------------------------- Fill tables -------------------------------------------------

INSERT INTO clients (FirstName, MIddleName, LastName, Address) VALUES ('Александров', 'Александр', 'Александрович', 'г. Москва, ул. Ленина, д.1');
INSERT INTO clients (FirstName, MIddleName, LastName, Address) VALUES ('Петров', 'Петр', 'Петрович', 'г. Москва, ул. Зеленая, д.8');
INSERT INTO clients (FirstName, MIddleName, LastName, Address) VALUES ('Степанов', 'Степан', 'Степанович', 'г. Москва, ул. Набережная, д.30');
INSERT INTO clients (FirstName, MIddleName, LastName, Address) VALUES ('Федоров', 'Федор', 'Федорович', 'г. Казань, ул. Гвардейцев, д.81');
INSERT INTO clients (FirstName, MIddleName, LastName, Address) VALUES ('Андреев', 'Андрей', 'Андреевич', 'г. Казань, ул. Гагарина, д.16');

INSERT INTO credit_types (Name, Description, Rate, TermDays) VALUES ('Новостройки в кредит', 'Описание кредита1', 11.81, 365);
INSERT INTO credit_types (Name, Description, Rate, TermDays) VALUES ('Новое авто в кредит', 'Описание кредита2', 6.21, 365);
INSERT INTO credit_types (Name, Description, Rate, TermDays) VALUES ('Кредит наличными без залога', 'Описание кредита3', 54.11, 365);
INSERT INTO credit_types (Name, Description, Rate, TermDays) VALUES ('Кредит наличными под залог', 'Описание кредита4', 17.83, 365);
INSERT INTO credit_types (Name, Description, Rate, TermDays) VALUES ('Вторичка в кредит', 'Описание кредита5', 13.77, 365);

INSERT INTO credits (ClientId, CreditTypeId, Amount, DateOfIssue, IsRepaid) VALUES (1, 1, 10000000, '01-03-2020 13:50', false);
INSERT INTO credits (ClientId, CreditTypeId, Amount, DateOfIssue, IsRepaid) VALUES (2, 2, 500000, '07-03-2021 12:40', true);
INSERT INTO credits (ClientId, CreditTypeId, Amount, DateOfIssue, IsRepaid) VALUES (3, 3, 1000000, '30-08-2019 13:50', false);
INSERT INTO credits (ClientId, CreditTypeId, Amount, DateOfIssue, IsRepaid) VALUES (4, 1, 700000, '07-03-2021 12:40', false);
INSERT INTO credits (ClientId, CreditTypeId, Amount, DateOfIssue, IsRepaid) VALUES (5, 2, 300000, '20-04-2020 12:40', true);