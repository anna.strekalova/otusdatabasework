﻿namespace OtusDatabaseWork.Entities
{
    /// <summary>
    /// Класс, представляющий модель сущности "Вид кредита"
    /// </summary>
    public class CreditType
    {
        /// <summary>
        /// ИД
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ставка
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Срок дней
        /// </summary>
        public int TermDays { get; set; }

        public CreditType()
        {
        }

        public CreditType(string name, string description, decimal rate, int termDays)
        {
            Name = name;
            Description = description;
            Rate = rate;
            TermDays = termDays;
        }

        public override string ToString()
        {
            return $"{Name}, {Description}, {Rate}, {TermDays}";
        }
    }
}
