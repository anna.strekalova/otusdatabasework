﻿namespace OtusDatabaseWork.Entities
{
    using System;

    /// <summary>
    /// Класс, представляющий модель сущности "Кредит"
    /// </summary>
    public class Credit
    {
        /// <summary>
        /// ИД
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        ///  ИД клиента
        /// </summary>
        public long? ClientId { get; set; }

        /// <summary>
        /// Клиент
        /// </summary>
        public Client Client { get; set; }

        /// <summary>
        /// ИД вида кредита
        /// </summary>
        public long? CreditTypeId { get; set; }

        /// <summary>
        /// Вид кредита
        /// </summary>
        public CreditType CreditType { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTime DateOfIssue { get; set; }

        /// <summary>
        /// Признак погашения
        /// </summary>
        public bool IsRepaid { get; set; }

        public Credit()
        {
        }

        public Credit(long? clientId, long? creditTypeId, decimal amount, DateTime dateOfIssue, bool isRepaid)
        {
            ClientId = clientId;
            CreditTypeId = creditTypeId;
            Amount = amount;
            DateOfIssue = dateOfIssue;
            IsRepaid = isRepaid;
        }

        public override string ToString()
        {
            var isRepaidText = IsRepaid ? "да" : "нет";
            return $"Кредит: Сумма: {Amount}. Дата выдачи: {DateOfIssue:dd.MM.yyyy}. Погашен: {isRepaidText} "
                + $"Клиент: {Client.FirstName} {Client.MiddleName} {Client.LastName}. Вид кредита: {CreditType.Name}.";
        }
    }
}
