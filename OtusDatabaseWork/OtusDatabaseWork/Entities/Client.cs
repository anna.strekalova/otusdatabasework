﻿namespace OtusDatabaseWork.Entities
{
    /// <summary>
    /// Класс, представляющий модель сущности "Клиент"
    /// </summary>
    public class Client
    {
        /// <summary>
        /// ИД
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///Имя
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }

        public Client()
        {
        }

        public Client(string firstName, string middleName, string lastName, string address)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            Address = address;
        }
       
        public override string ToString()
        {
            return $"{FirstName} {MiddleName} {LastName}, {Address}";
        }
    }
}
